import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { expect } from 'chai';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

  describe('#getUsers', async () => {
    it('should get users', async () => {
      request(app.getHttpServer())
        .get('/users')
        .expect(200)
        .then((rsp) => {
          const users = rsp.body;
          expect(users).to.exist;
          expect(users.length).to.equal(1);
        });
    });
  });
});
