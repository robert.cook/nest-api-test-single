import { randomUUID } from 'crypto';

export interface UserFields {
  id?: string;
  email: string;
  profile?: Record<string, unknown>;
}

export class User implements UserFields {
  id: string;
  email: string;
  profile?: Record<string, unknown>;
  constructor(data?: UserFields) {
    this.id = data?.id || randomUUID();
    this.email = data?.email || '';
  }
}
