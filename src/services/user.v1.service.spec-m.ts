import { Test, TestingModule } from '@nestjs/testing';
import { DatastoreModule, UserDatastoreV1 } from '../datastore';
import { UserServiceV1 } from './user.v1.service';
import * as sinon from 'sinon';
import { expect } from 'chai';
import { User } from '../models';
import { randomUUID } from 'crypto';
import { randEmail } from '@ngneat/falso';

/** Mocha / Chai / Sinon */
describe('UserServiceV1', () => {
  let sandbox: sinon.SinonSandbox;
  let service: UserServiceV1;
  let userDatastore: UserDatastoreV1;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatastoreModule],
      providers: [UserServiceV1],
    }).compile();

    service = module.get<UserServiceV1>(UserServiceV1);
    userDatastore = module.get<UserDatastoreV1>(UserDatastoreV1);
  });

  describe('#getUsers', async () => {
    it('should get users', async () => {
      sandbox
        .stub(userDatastore, 'getUsers')
        .callsFake(
          () =>
            new Promise((resolve) =>
              resolve([new User({ id: randomUUID(), email: randEmail() })]),
            ),
        );
      const users = await service.getUsers();
      expect(users).to.exist;
      expect(users.length).to.equal(1);
    });
  });
});
