import { Module } from '@nestjs/common';
import { DatastoreModule } from '../datastore';
import { UserServiceV1 } from './user.v1.service';

@Module({
  imports: [DatastoreModule],
  providers: [UserServiceV1],
  exports: [UserServiceV1],
})
export class ServicesModule {}
