import { UserDatastoreV1 } from '../datastore';
import { User } from '../models';
import { Injectable } from '@nestjs/common';

@Injectable()
export class UserServiceV1 {
  constructor(private readonly userDatastore: UserDatastoreV1) {}

  async getUsers(): Promise<User[]> {
    const users = await this.userDatastore.getUsers();
    return users;
  }
}
