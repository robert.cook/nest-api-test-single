/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum userType {
  ADMIN = 'admin',
  DEVELOPER = 'developer',
  VIEWER = 'viewer',
}
