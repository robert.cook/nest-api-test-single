/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { userType } from './userType';

/**
 * A user
 */
export type user = {
  /**
   * The user id
   */
  id?: string;
  /**
   * The user email
   */
  email?: string;
  userType?: userType;
};

