import { Module } from '@nestjs/common';
import { UserDatastoreV1 } from './user.v1.datastore';

const dynamicImport = async (packageName: string) =>
  new Function(`return import('${packageName}')`)();

@Module({
  providers: [
    {
      provide: UserDatastoreV1,
      async useFactory() {
        return new UserDatastoreV1((await dynamicImport('got')).default);
      },
    },
    // UserDatastoreV1,
  ],
  exports: [UserDatastoreV1],
})
export class DatastoreModule {}
