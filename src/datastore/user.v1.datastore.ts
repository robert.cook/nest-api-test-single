import { Injectable } from '@nestjs/common';
import { User } from '../models';
import { createHash } from 'crypto';
import { Got } from 'got';

@Injectable()
export class UserDatastoreV1 {
  constructor(private readonly got: Got) {}

  async getUsers() {
    const user = new User({ email: 'robert.cook@everactive.com' });
    user.profile = await this.getGravatarProfile(user.email);
    return [user];
  }

  private async getGravatarProfile(
    email: string,
  ): Promise<Record<string, unknown>> {
    const hash = createHash('md5').update(email).digest('hex');
    const response = await this.got.get(
      `https://www.gravatar.com/${hash}.json`,
    );
    return JSON.parse(response?.body || '{}');
  }
}
