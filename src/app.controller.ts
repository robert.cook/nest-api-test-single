import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { UserServiceV1 } from './services';
import { User } from './models';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly userService: UserServiceV1,
  ) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('users')
  async getUsers(): Promise<User[]> {
    const users = await this.userService.getUsers();
    return users;
  }
}
